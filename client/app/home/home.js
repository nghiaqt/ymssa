angular.module('ymssaApp').config(function($stateProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: '/app/home/views/index.html',
      controller: 'HomeCtrl'
    })
});
