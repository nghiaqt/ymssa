angular.module('ymssaApp')
  .directive('mixitup', ['$timeout',function($timeout) {
    return {
      restrict: 'AE',
      link: function($scope,element,attrs) {
        $timeout(function() {$(element).mixItUp()},1000);
      }
    }
  }])
  .directive('bannerHome',[function($timeout) {
    return {
      restrict: 'C',
      link: function($scope,element,attrs) {
        var heightBanner = $(window).height();
        $(element).css('min-height', heightBanner);
      }
    }
  }])
  .directive('customScrollBar',[function() {
    return {
      restrict: 'AE',
      link: function($scope,element,attrs) {
        $scope.$watch(function() {
          return $(element).parent().height()
        },function(value) {
          $(element).mCustomScrollbar({
            setHeight: "auto",
            theme: "inset-2-dark"
          });
          $(element).css('height', value - 80);
        });

      }
    }
  }])
  .directive('slick',[function() {
    return {
      restrict: 'AE',
      link: function($scope,element) {
        $(element).slick({
          dots: true,
          infinite: false,
          speed: 300,
          slidesToShow: 10,
          slidesToScroll: 10,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 8,
                slidesToScroll: 8,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
      }
    }
  }])
  .directive('toogleHeader',[function() {
    return {
      restrict: 'C',
      link: function($scope,element) {
        $(element).bind('click',function () {
          $('.header').show();
        });
        $('.close-header').bind('click',function () {
          $('.header').hide();
        });
      }
    }
  }])
  .directive('readmore',[function() {
    return {
      restrict: 'C',
      link: function($scope,element) {
        $(element).bind('click',function() {
          $('.announcements').addClass('active');
          $(this).hide();
        });
        $('.close-readmore').click(function () {
          $('.announcements').removeClass('active');
          $(element).show();
        });
      }
    }
  }])
  .directive('combobox',[function() {
    return {
      restrict: 'AE',
      link: function($scope,element) {
        $(element).combobox();
      }
    }
  }]);